import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";

/*
  Generated class for the SoundProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SoundProvider {
  private subject = new Subject<any>();

  constructor(public http: HttpClient) {
  }

  sendMessage(condition: boolean) {
    console.log(condition);
    this.subject.next({ cond: condition });
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

}
