import {Component} from '@angular/core';
import {IonicPage, LoadingController, Platform, NavController} from 'ionic-angular';
import * as firebase from "firebase";
import {AngularFireAuth} from "angularfire2/auth";
import {GooglePlus} from "@ionic-native/google-plus";
import {HttpService} from "../../provaders/http.service";
import {Facebook} from "@ionic-native/facebook";
// import AuthProvider = firebase.auth.AuthProvider;

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  user: any;
  errors: any;
  data: any;


  userData = {
    name: '',
    level_id: 1,
    social_id: '',
    email: ''
  };
  userShowData = {
    displayName: "",
    imageUrl: "",
    email: ""
  };

  UserDataInt = {
    displayName: "",
    email: "",
    userId: "",
    imageUrl: "",
  };

  constructor(private afAuth: AngularFireAuth,
              private GoogleSignin: GooglePlus,
              private platform: Platform,
              public https: HttpService,
              public loadingCtrl: LoadingController,
              public facebook: Facebook,
              private navCtrl: NavController
             ) {
    let userDataLoclal = JSON.parse(window.localStorage.getItem('userData'));
    if (userDataLoclal !== null) {
      console.log(userDataLoclal.displayName, 'UserData');
      this.userShowData.displayName = userDataLoclal.displayName;
      this.userShowData.email = userDataLoclal.email;
      this.userShowData.imageUrl = userDataLoclal.imageUrl;
    } else {
      console.log('UserData of NULL');
    }
  }


  GoogleLogin() {

    if (this.platform.is('cordova')) {
      console.log('if');
      this.GoogleSignin.login({
        'webClientId': '672138296908-jn9qv3a2478suu9pdle1qdefjeluffb6.apps.googleusercontent.com',
        'offline': true,
        'scopes': 'profile email'
      })
        .then(res => {

          this.UserDataInt.userId = res.userId;
          this.UserDataInt.displayName = res.displayName;
          this.UserDataInt.email = res.email;
          this.UserDataInt.imageUrl = res.imageUrl;

          window.localStorage.setItem('userData', JSON.stringify(this.UserDataInt));
          this.data = this.afAuth.auth;
          // this.user = this.afAuth.authState;
          this.userShowData.displayName = res.displayName;
          this.userShowData.email = res.email;
          this.userShowData.imageUrl = res.imageUrl;

          this.userData.name = res.displayName;
          this.userData.social_id = res.userId;
          this.userData.email = res.email;
          this.errors = this.userData;

          this.https.createUser(this.userData).subscribe((data) => {
            window.localStorage.setItem('GameData', JSON.stringify(data));
            location.reload();
            this.preLoader();
          });

        }).catch(error => {
        this.errors = error;
      });
    } else {
      const provider = new firebase.auth.GoogleAuthProvider();
      this.afAuth.auth.signInWithPopup(provider).then(res => {
        console.log(res.additionalUserInfo.profile.id, 'Profile');
        this.errors = res;
        this.data = this.afAuth.auth;
        console.log(res.user, "USERDATA***");

        this.UserDataInt.userId = res.additionalUserInfo.profile.id;
        this.UserDataInt.displayName = res.user.displayName;
        this.UserDataInt.email = res.user.email;
        this.UserDataInt.imageUrl = res.user.photoURL;

        window.localStorage.setItem('userData', JSON.stringify(this.UserDataInt));


        this.userShowData.displayName = res.user.displayName;
        this.userShowData.email = res.user.email;
        this.userShowData.imageUrl = res.additionalUserInfo.profile.picture;

        this.userData.name = res.user.displayName;
        this.userData.social_id = res.additionalUserInfo.profile.id;
        this.userData.email = res.user.email;
        this.errors = this.userData;
        console.log(this.userData);
        this.https.createUser(this.userData).subscribe((data) => {
          console.log(data, 'createUser Data');
          window.localStorage.setItem('GameData', JSON.stringify(data));

          console.log(data,'data from createUser ...')
          location.reload();
          this.preLoader();
        });

      });
    }
  }



  loginFacebook(): Promise<any>  {
    return this.facebook.login(['email'])
      .then( response => {
        const facebookCredential = firebase.auth.FacebookAuthProvider
          .credential(response.authResponse.accessToken);

        firebase.auth().signInWithCredential(facebookCredential)
          .then( res => {
            this.UserDataInt.userId = res.uid;
                this.UserDataInt.displayName = res.displayName;
                this.UserDataInt.email = res.email;
                this.UserDataInt.imageUrl = res.photoURL;

                window.localStorage.setItem('userData', JSON.stringify(this.UserDataInt));
                this.data = res;
                this.userShowData.displayName = res.displayName;
                this.userShowData.email = res.email;
                this.userShowData.imageUrl = res.photoURL;

                this.userData.name = res.displayName;
                this.userData.social_id = res.uid;
                this.userData.email = res.email;


                this.https.createUser(this.userData).subscribe((data) => {
                  this.data = data;
                  window.localStorage.setItem('GameData', JSON.stringify(data));
                  location.reload();
                  this.preLoader();
                });
          });

      }).catch((error) => { this.data = JSON.stringify(error) });
    // this.facebook.login(['email']).then((loginResponse) => {
    //   let credentical = firebase.auth.FacebookAuthProvider.credential(loginResponse.authResponse.accessToken);
    //   firebase.auth().signInWithCredential(credentical).then((res) => {
    //
    //     this.UserDataInt.userId = res.uid;
    //     this.UserDataInt.displayName = res.displayName;
    //     this.UserDataInt.email = res.email;
    //     this.UserDataInt.imageUrl = res.photoURL;
    //
    //     window.localStorage.setItem('userData', JSON.stringify(this.UserDataInt));
    //     this.data = res;
    //     this.userShowData.displayName = res.displayName;
    //     this.userShowData.email = res.email;
    //     this.userShowData.imageUrl = res.photoURL;
    //
    //     this.userData.name = res.displayName;
    //     this.userData.social_id = res.uid;
    //     this.userData.email = res.email;
    //
    //
    //     this.https.createUser(this.userData).subscribe((data) => {
    //       this.data = data;
    //       window.localStorage.setItem('GameData', JSON.stringify(data));
    //       location.reload();
    //       this.preLoader();
    //     });
    //
    //   })
    // })
  }

  signOut() {
    this.data = this.afAuth.auth;
    this.errors =  this.afAuth.auth.signOut();
    this.afAuth.auth.signOut();
    this.userShowData = {
      displayName: "",
      imageUrl: "",
      email: ""
    };
    this.preLoader();
    window.localStorage.removeItem('userData');
    if (this.platform.is('cordova')) {
      window.localStorage.removeItem('userData');
      this.afAuth.auth.signOut();
      this.preLoader();
    }
  }

  //change rout for tabs
  goToPage(path){
    this.navCtrl.push(path);
  }

  preLoader() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="loading-custom-spinner-container">
          <div class="box">
            <img src="assets/imgs/logo-round.png" alt="spinner">
          </div>
        </div>`,
      duration: 1500
    });
    loading.present();
  }

}
