import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {HttpService} from "../../provaders/http.service";
import {SocialSharing} from "@ionic-native/social-sharing";
import {SoundProvider} from "../../provaders/sound/sound";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  public soundSettings = true;

  constructor(public navCtrl: NavController,
              public http: HttpService,
              public socialSharing: SocialSharing,
              public sound: SoundProvider) {
  }

  updateItem(condition) {
    console.log(condition);
    this.sound.sendMessage(condition);
  }

  shareFacebook() {
    let message = 'Hey! I am playing LevelUp ! Join me below :) Here you can download the game Link';
    let image = 'assets/imgs/Logo1.png';
    let url = '';

    this.socialSharing.share(message, '', image, url).then(() => {
      console.log('great');
    }).catch(() => {
      console.log('wrong');
    });
  }

  //change rout for tabs
  goToPage(path){
    this.navCtrl.push(path);
  }

  data: any;
  public event = {
    timeStarts: '07:43',
  }

}
