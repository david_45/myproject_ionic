import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the LvlChangePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-lvl-change',
  templateUrl: 'lvl-change.html',
})
export class LvlChangePage {
  game_condition: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.game_condition = navParams.get('condition');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LvlChangePage');
  }

  dismiss() {
    let data = {
      closed: true
    };
    this.viewCtrl.dismiss(data);
  }

}
