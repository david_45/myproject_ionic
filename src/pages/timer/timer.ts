import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { ShopModalPage } from '../shop-modal/shop-modal';

/**
 * Generated class for the TimerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-timer',
  templateUrl: 'timer.html',
})
export class TimerPage {
  timerM: any;
  timerS: any;
  duration: any;

  constructor(public navCtrl: NavController, public params: NavParams, private myModal: ModalController, public viewCtrl: ViewController) {
    console.log(params.get('timer'));
    this.duration = params.get('timer');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TimerPage');
    this.timerOne(this.duration);
  }

  shopModal() {
    let shop = this.myModal.create(ShopModalPage);

    shop.present();
  }

  timerOne(duration: number) {

    let timer = duration;
    let minutes;
    let seconds;
    let timers = setInterval(() => {
      minutes = Math.floor(timer / 60);
      seconds = Math.ceil(timer % 60);
      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;
      console.log(minutes, seconds);
      this.timerM = minutes;
      this.timerS = seconds;
      if (minutes === '00' && seconds === '00') {
        this.viewCtrl.dismiss();
        clearInterval(timers);
        return;
      }


      if (--timer < 0) {
        return;
      }


    }, 1000);
  }

}
