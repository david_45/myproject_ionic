import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular'
import { LoginPage } from "../login/login";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {


  constructor(private navCtrl: NavController){
  }

  //

  ngOnInit(){
    this.roteToLogIn();
  }

  roteToLogIn(){
    let userData = localStorage.getItem('userData');
    console.log(userData)
    if (userData === null) {
      this.navCtrl.push(LoginPage);
    }
  }

  //change rout for tabs
  goToPage(path){
    this.navCtrl.push(path);
  }

  goToGame(){
    this.navCtrl.push('Game');
  }

}
