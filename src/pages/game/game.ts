import {Component, OnInit, ViewEncapsulation, ElementRef} from '@angular/core';
import {IonicPage, LoadingController, NavController, ModalController} from 'ionic-angular';
import {HttpService} from "../../provaders/http.service";
import {LoginPage} from "../login/login";
import { trigger,state,style,transition,animate } from '@angular/animations';
import { ShopModalPage } from '../shop-modal/shop-modal';
import { LvlChangePage } from '../lvl-change/lvl-change';
import { TimerPage } from '../timer/timer';
import { Vibration } from '@ionic-native/vibration';
import {SoundProvider} from "../../provaders/sound/sound";


/**
 * Generated class for the GamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-game',
  templateUrl: 'game.html',
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('ScaleState', [
      state('inactive', style({
        opacity: '1',
        transform: 'scale(1)'
      })),
      state('active',   style({
        opacity: '1',
        transform: 'scale(1.2)'
      })),
      transition('inactive => active', animate('200ms ease-in')),
      transition('active => inactive', animate('200ms ease-out')),
      transition('void => inactive', [
        style({
          opacity: '0',
          transform: 'scale(0) translateY(-100%)'
        }),
        animate('200ms cubic-bezier(.8,1.57,.76,.75)', style({
          opacity: '1',
          transform: 'scale(1) translateY(0)'
        }))
      ]),
      transition('* => inactive', [
        style({
          opacity: '0',
          transform: 'scale(0)'
        }),
        animate('200ms cubic-bezier(.8,1.57,.76,.75)', style({
          opacity: '1',
          transform: 'scale(1)'
        }))
      ]),
      transition('active => *', [
        style({
          opacity: '1',
          transform: 'scale(1.2)'
        }),
        animate('200ms cubic-bezier(.8,1.57,.76,.75)', style({
          opacity: '0',
          transform: 'scale(1.3) translateY(20%)'
        }))
      ])
    ]),
    trigger('LeftIn', [
      transition('void => *',[
        style({
          opacity: '0',
          transform: 'translateX(-100%)'
        }),
        animate('1000ms cubic-bezier(.8,1.57,.76,.75)', style({
          opacity: '1',
          transform: 'translateX(0)'
        }))
      ])
    ]),
    trigger('rightIn', [
      transition('void => *',[
        style({
          opacity: '0',
          transform: 'translateX(100%)'
        }),
        animate('1000ms cubic-bezier(.8,1.57,.76,.75)', style({
          opacity: '1',
          transform: 'translateX(0)'
        }))
      ])
    ]),
    trigger('Bag', [
      transition('inactive => active',[
        style({
          opacity: '0',
          transform: 'translateY(0) scale(1)'
        }),
        animate('1000ms cubic-bezier(.32,-0.06,.27,-0.4)', style({
          opacity: '1',
          transform: 'translateY(110%) scale(1.2)'
        }))
      ]),
      transition('active => inactive', [
        style({
          opacity: '0',
          transform: 'translateY(110%)'
        }),
        animate('200ms', style({
          opacity: '0',
          transform: 'translateY(0) scale(0)'
        }))
      ])
    ]),
    trigger('Points', [
      transition('inactive => active',[
        style({
          transform: 'translateY(0) scale(1)'
        }),
        animate('1000ms cubic-bezier(.64,1.24,.64,.9)', style({
          transform: 'translate(18vh, -130vw) scale(0.5)',
          opacity: '0',
        }))
      ]),
      transition('active => inactive', [
        style({
          opacity: '0',
          transform: 'translateY(110%)'
        }),
        animate('200ms', style({
          opacity: '0',
          transform: 'translateY(0) scale(0)'
        }))
      ])
    ]),
  ]
})
export class GamePage implements OnInit {
  leftRoom = [];
  reghtRoom = [];
  leftRoomSum = [];
  reghtRoomSum = [];
  indexElements: number[] = [];
  indexElementsLeft: number[] = [];
  GameLevel: number;
  leftRoomCount: number;
  reghtRoomCount: number;
  GameBal: number;
  WinnerPoints = 0;
  GameLive: number;
  color = '#c0c0c0';
  timerM: any;
  timerS: any;
  intervalLive: any;
  LeftNumber_left: any;
  LeftNumber_Right: any;
  state: 'inactive';
  lifeBg = [
    {item: 'live'},
    {item: 'live'},
    {item: 'live'}
  ];
  bagState: string = 'inactive';
  PointsState: string = 'inactive';
  PointsCount: any;
  animTimer: any;
  currentUrl: string;
  subscription: boolean;


  RightNumber_left: any;
  RightNumber_Right: any;
  Sum: any;
  SumRight: any;
  testData: any;
  GameData: any;
  number = 0;
  timeLives = 0;
  timers: any;
  loaderCondition: boolean;
  soundCondition = false;

  constructor(private http: HttpService, private navCtrl: NavController,
              public loadingCtrl: LoadingController, public https: HttpService,private myModal: ModalController,
              public elementRef: ElementRef,
              private vibration: Vibration,
              public sound: SoundProvider) {


    let islogin = localStorage.getItem('userData');

    if (islogin === null) {
      // this.navCtrl.push(LoginPage);
    } else {
      this.gameData();
    }

  }


  gameData() {
    this.loaderCondition = true;
    let gameData = JSON.parse(localStorage.getItem('GameData'));
    this.animTimer = gameData.timer;
    this.timerOneAllLevels(gameData.timer);
    this.https.getTime(gameData.social_id).subscribe((data: any) => {
      window.localStorage.setItem('GameData', JSON.stringify(data));
      this.setData(data);
    });

    if (gameData.life === 0) {
      this.http.blockTime(gameData.social_id).subscribe((data: any) => {
        if (data.timeout !== null) {
          setTimeout(() => {
            this.timerOne(data.timeout);
          },500)
        }
      });

    }
  }

  liveIndicator() {
    this.lifeBg = [
      {item: 'live'},
      {item: 'live'},
      {item: 'live'}
    ];

    if(this.GameLive > 1 ){
      for (let i = 0; i < this.GameLive - 1; i++) {
        if (i >= 3){
          break;
        }
        this.lifeBg[i].item = 'live_active'
      }
    }
  }

  shopModal() {
    let shop = this.myModal.create(ShopModalPage);

    shop.present();
  }

  lvlChangeModal(condData: boolean) {
    clearInterval(this.timers);
    this.animTimer = 0;
    this.loaderCondition = false;
    let lvlData = {
      condition: condData
    };
    let closed = null;
    let shop = this.myModal.create(LvlChangePage, lvlData);

    shop.onDidDismiss(data => {
      closed = data.closed;
      this.loaderCondition = true;
      if (closed && lvlData.condition) {
        this.animTimer = 0;
        this.NexLevel();
      } else if (closed && !lvlData.condition) {
        this.animTimer = 0;
        this.WinnerPoints = 0;
        this.gameData();
      }
    });

    shop.present();
  }

  rotateAnimation() {
   return {'animation': `rotate ${this.animTimer}s 1500ms infinite linear`};
  }

  rotateAnimationNone() {
    return {'animation': ''};
  }

  rotateAnimationFill() {
    return {'animation-duration': `${this.animTimer}s`};
  }

  spinnerAnimation() {
    return {'animation': `rota ${this.animTimer}s 1500ms linear infinite`};
  }

  fillerAnimation() {
    return {'animation': `opa ${this.animTimer}s steps(1,end) 1500ms infinite reverse`};
  }

  maskAnimation() {
    return {'animation': `opa ${this.animTimer}s steps(1,end) 1500ms infinite`};
  }


  ngOnInit() {
    this.preLoader();
    this.getData();
    this.sound.getMessage().subscribe(message => {
      console.log(message);
      this.soundCondition = message;
      // this.soundCondition = message;
    });
    console.log(this.soundCondition);
  }

  getData() {

    this.leftRoom = [];
    this.leftRoomSum = [];
    this.reghtRoom = [];
    this.reghtRoomSum = [];
    this.indexElements = [];
    this.indexElementsLeft = [];
    this.lifeBg = [];

    let GameData = JSON.parse(localStorage.getItem('GameData'));
    if (GameData !== undefined) {
      if(GameData.lable_1){
        this.leftRoom = GameData.lable_1.map( index => ({
          num: index,
          state: 'inactive'
        }));
        this.reghtRoom = GameData.lable_2.map( index => ({
          num: index,
          state: 'inactive'
        }));
      }
      this.GameLevel = GameData.level;
      this.leftRoomCount = GameData.lable_1_step;
      this.reghtRoomCount = GameData.lable_2_step;
      this.GameBal = GameData.points;
      this.GameLive = GameData.life;

      this.getThime(GameData.social_id);
      this.liveIndicator();

      if (this.timeLives === 0) {
        this.timeLives = GameData.timer;
      };
    }
  }

  animEnd(){
    this.PointsState = 'inactive';
  }

  //change rout for tabs
  goToPage(path){
    this.navCtrl.setRoot(path);
    clearInterval(this.timers);
  }

  getThime(social_id) {
    this.http.getTime(social_id).subscribe(
      (data: any) => {
        return data
      }, err => {
        return err;
      });
  }


  PointBagAnimation(points) {
    this.bagState = 'active';
    setTimeout(() => {
      this.WinnerPoints += points;
      this.bagState = 'inactive';
    },1200)
  }

  /** --------------------------------- Play Music Click ----------------------------- **/
  musicClickPlay(){
    let player = <HTMLAudioElement>document.getElementById("music_click");
    player.play();
  }
  /** --------------------------------- Play Music Point ----------------------------- **/
  musicPointPlay(){
    let player = <HTMLAudioElement>document.getElementById("music_point");
    player.play();
  }

  /** --------------------------------- LEFT CLICK ----------------------------- **/
  leftRoomClick(element, index) {
    this.vibration.vibrate(150);
    this.musicClickPlay();
    this.leftRoom[index].state = (this.leftRoom[index].state === 'active' ? 'inactive' : 'active');
    setTimeout(() =>{
      if (this.leftRoomCount === 0) {
        element.stopPropagation();
        return;
      } else {
        this.LeftGame(element, index);
      }

      if (this.leftRoomCount === 0 && this.reghtRoomCount === 0) {
        this.SumGame();
      }
    },1000)
  }

  /** --------------------------------- RIGHT CLICK ----------------------------- **/
  reghtRoomClick(element, index) {
    this.vibration.vibrate(150);
    this.musicClickPlay();
    this.reghtRoom[index].state = (this.reghtRoom[index].state === 'active' ? 'inactive' : 'active');

    setTimeout(() => {
      if (this.reghtRoomCount === 0) {
        element.stopPropagation();
        return;
      } else {
        this.RightGame(element, index);
      }

      if (this.leftRoomCount === 0 && this.reghtRoomCount === 0) {
        this.SumGame();
      }
    },800)
  }

  /** ----------------------------------------- LEFT ----------------------------- **/
  LeftGame(element, index) {
    if (element.toElement.classList[1] === 'defleft') {
      this.Sum = '';
      this.leftRoomSum.push(parseInt(element.target.innerHTML));

      if (this.LeftNumber_left === undefined) {
        this.LeftNumber_left = parseInt(element.target.innerHTML);
      } else {
        this.LeftNumber_Right = parseInt(element.target.innerHTML);
      }
      this.indexElementsLeft.push(parseInt(index));
      element.toElement.classList.remove("defleft");

      if (this.leftRoomSum.length === 2) {
        this.leftRoomCount--;
        let Sum = this.leftRoomSum.reduce((a, b) => a + b);
        this.Sum = Sum;
        this.PointsCount = Sum;
        this.PointBagAnimation(Sum);


        this.addClassLeft();

        this.indexElementsLeft.sort((a, b) => {
          return b - a;
        });

        let indexOne = this.indexElementsLeft[0];
        let indexTwo = this.indexElementsLeft[1];

        this.leftRoom[indexOne].state = '*';
        this.leftRoom[indexTwo].state = '*';

        setTimeout(()=> {
          this.indexElementsLeft.map((element) => {
            this.leftRoom.splice(element, 1);
          });

          this.indexElementsLeft = [];

          setTimeout(() => {
            this.LeftNumber_left = undefined;
            this.LeftNumber_Right = undefined;
          }, 300);

          if (("" + Sum).split("").length === 2) {
            let FirstNumber = +("" + Sum).split("")[0];
            let TowNumber = +("" + Sum).split("")[1];
            let SumTow = FirstNumber + TowNumber;
            let obj = {
              num: SumTow,
              state: 'inactive'
            };
            this.leftRoom.push(obj);
          } else {
            let obj = {
              num: Sum,
              state: 'inactive'
            };
            this.leftRoom.push(obj);
          }
        },500);

        if (this.leftRoomCount !== 0) {
          this.leftRoomSum = [];
        }

        if (this.leftRoomCount === 0 && this.leftRoom.length >= 2) {
          setTimeout(()=> {
            let finaldata = this.leftRoom[this.leftRoom.length - 1];
            this.leftRoom = [];
            this.leftRoom.push(finaldata);
          },500);
        }
      }
    } else {
      this.LeftNumber_left = undefined;
      this.LeftNumber_Right = undefined;
      element.toElement.classList.add("defleft");
      this.leftRoomSum = [];
      this.indexElementsLeft = [];
    }

  }

  /** ----------------------------------------- RIGHT ----------------------------- **/
  RightGame(element, index) {
    if (element.toElement.classList[1] === 'defright') {
      this.SumRight = "";
      this.reghtRoomSum.push(parseInt(element.target.innerHTML));
      this.indexElements.push(parseInt(index));
      if (this.RightNumber_left === undefined) {
        this.RightNumber_left = parseInt(element.target.innerHTML);
      } else {
        this.RightNumber_Right = parseInt(element.target.innerHTML);
      }
      element.toElement.classList.remove("defright");

      if (this.reghtRoomSum.length === 2) {
        this.reghtRoomCount--;
        let Sum = this.reghtRoomSum.reduce((a, b) => a + b);
        this.SumRight = Sum;
        this.PointsCount = Sum;
        this.PointBagAnimation(Sum);
        this.addClassRight();
        this.indexElements.sort((a, b) => {
          return b - a;
        });

        let indexOne = this.indexElements[0];
        let indexTwo = this.indexElements[1];

        this.reghtRoom[indexOne].state = '*';
        this.reghtRoom[indexTwo].state = '*';

        setTimeout(() => {
          this.indexElements.map((element) => {
            this.reghtRoom.splice(element, 1);
          });

          this.indexElements = [];
          setTimeout(() => {
            this.RightNumber_left = undefined;
            this.RightNumber_Right = undefined;
          }, 300);


          if (("" + Sum).split("").length === 2) {
            let FirstNumber = +("" + Sum).split("")[0];
            let TowNumber = +("" + Sum).split("")[1];
            let SumTow = FirstNumber + TowNumber;
            let obj = {
              num: SumTow,
              state: 'inactive'
            };
            this.reghtRoom.push(obj);
          } else {
            let obj = {
              num: Sum,
              state: 'inactive'
            };
            this.reghtRoom.push(obj);
          }
        },500)

        if (this.reghtRoomCount !== 0) {
          this.reghtRoomSum = [];
        }

        if (this.reghtRoomCount === 0 && this.reghtRoom.length >= 2) {
          setTimeout(() => {
            let finaldata = this.reghtRoom[this.reghtRoom.length - 1];
            this.reghtRoom = [];
            this.reghtRoom.push(finaldata);
          },500);
        }

      }
    } else {
      this.RightNumber_left = undefined;
      this.RightNumber_Right = undefined;
      element.toElement.classList.add("defright");
      this.reghtRoomSum = [];
      this.indexElements = [];
    }

  }


  /** Finishing Sum **/
  SumGame() {
    setTimeout(()=>{
      if (this.leftRoom[0].num > this.reghtRoom[0].num) {
        let Sum = this.leftRoom[0].num - this.reghtRoom[0].num;
        this.GameResult(Sum);
      } else {
        let Sum = this.reghtRoom[0].num - this.leftRoom[0].num;
        this.GameResult(Sum);
      }
    },900)
  }

  /** GameResult **/
  GameResult(result) {
    this.musicPointPlay()
    if (result === 1) {
      this.LeftNumber_left = undefined;
      this.LeftNumber_Right = undefined;
      this.RightNumber_left = undefined;
      this.RightNumber_Right = undefined;
      setTimeout(()=>{
        this.GameBal += this.WinnerPoints + this.leftRoom[0].num + this.reghtRoom[0].num;
        this.PointsState = 'active';
        this.WinnerPoints = 0;
      },900)
      this.lvlChangeModal(true);
    } else {
      this.lvlChangeModal(false);
      this.WinnerPoints = 0;
      if (this.GameLive === 0) {
        let gameData = JSON.parse(localStorage.getItem('GameData'));
        this.http.blockTime(gameData.social_id).subscribe((data: any) => {
          // this.testData = data;
          if (data.life === 0) {
            setTimeout(() => {
              this.timerOne(data.timeout);
            },500)
          }
        });
      }
      let GameData = JSON.parse(localStorage.getItem('GameData'));
      this.timerOneAllLevels(GameData.timer);
      clearInterval(this.timers);

      this.http.blockTime(GameData.social_id).subscribe((data: any) => {
        if (data.message !== 'Your Block') {
          setTimeout(()=>{
            window.localStorage.setItem('GameData', JSON.stringify(data));
            this.getData();
          },500)
        } else {
          alert('Your Block');
        }
      });

    }
  }

  /** Level Next Function **/
  NexLevel() {

    let GameData = JSON.parse(localStorage.getItem('GameData'));

    let UserData = JSON.parse(localStorage.getItem('userData'));
    if (GameData !== null && UserData !== null) {
      let dataUser = {
        "level_id": GameData.level_id,
        "points": this.GameBal
      };
      this.http.Updatelevels(UserData.userId, dataUser).subscribe((data: any) => {
        if (data !== undefined) {

          this.leftRoom = [];
          this.leftRoomSum = [];
          this.reghtRoom = [];
          this.reghtRoomSum = [];
          this.indexElements = [];
          this.indexElementsLeft = [];
          this.WinnerPoints = 0;

          window.localStorage.setItem('GameData', JSON.stringify(data));
          let GameData = JSON.parse(localStorage.getItem('GameData'));
          clearInterval(this.timers);
          this.timerOneAllLevels(GameData.timer);
          this.setData(data);
        }

      });
    }
  }

  /** Add  Left Style class **/
  addClassLeft() {
    let div: any = document.getElementsByClassName('divsleft');
    for (let element of div) {
      element.classList.add("defleft");
    }
    return;
  }

  /** Add and Remove Right Style class **/
  addClassRight() {
    let div: any = document.getElementsByClassName('divsright');
    for (let element of div) {
      element.classList.add("defright");
    }
    return;
  }


  timerOne(duration: number) {
    this.loaderCondition = false;
    let data = {
      timer: duration
    };
    clearInterval(this.timers);

    let timer = this.myModal.create(TimerPage, data);

    timer.present();

    timer.onDidDismiss(data => {
      this.loaderCondition = true;
      this.animTimer = 0;
      let GameData = JSON.parse(localStorage.getItem('GameData'));
      this.http.funishBlockTime(GameData.social_id).subscribe(data => {
        window.localStorage.setItem('GameData', JSON.stringify(data));
        console.log(data['life']);
        this.GameLive = data['life'];
        this.leftRoom = data['lable_1'];
        this.reghtRoom = data['lable_2'];
        this.GameBal = data['points'];
        this.GameLevel = data['level_id'];
        this.leftRoomCount = data['lable_1_step'];
        this.reghtRoomCount = data['lable_2_step'];
        this.timerOneAllLevels(data['timer']);
        this.setData(data);
        this.liveIndicator();
      });
    });
  }


  timerOneAllLevels(duration: number) {
    let timer = duration;
    let minutes;
    let seconds;
    this.timers = setInterval(() => {
      minutes = Math.floor(timer / 60);
      seconds = Math.ceil(timer % 60);
      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;
      this.timerM = minutes;
      this.timerS = seconds;
      if (minutes === '00' && seconds === '00') {
        this.lvlChangeModal(false);
        let gameData = JSON.parse(localStorage.getItem('GameData'));

        this.http.blockTime(gameData.social_id).subscribe((data: any) => {
          if (data.life === 0) {
            setTimeout(() => {
              this.timerOne(data.timeout);
            },500)
            return;
          };

          window.localStorage.setItem('GameData', JSON.stringify(data));
          this.GameLive = data['life'];
          this.leftRoom = data.lable_1.map( index => ({
            num: index,
            state: 'inactive'
          }));
          this.reghtRoom = data.lable_2.map( index => ({
            num: index,
            state: 'inactive'
          }));
          this.liveIndicator();
          this.GameBal = data['points'];
          this.GameLevel = data['level_id'];
          this.leftRoomCount = data['lable_1_step'];
          this.reghtRoomCount = data['lable_2_step'];
          this.timeLives = data['timer'];
          // this.timerOneAllLevels(this.timeLives);
          this.reghtRoomSum = [];
          this.leftRoomSum = [];
        });
        clearInterval(this.timers);
        return;
      }


      if (--timer < 0) {
        return;
      }


    }, 1000);
  }

  // setLiveTime() {
  //   let userData = {
  //     name: '',
  //     level_id: 1,
  //     social_id: '',
  //     email: ''
  //   };
  //   this.intervalLive = setInterval(() => {
  //     let userDataLocal = JSON.parse(localStorage.getItem('userData'));
  //     userData.name = userDataLocal.displayName;
  //     userData.social_id = userDataLocal.userId;
  //     userData.email = userDataLocal.email;
  //     this.https.funishBlockTime(userDataLocal.userId).subscribe((data) => {
  //       this.GameLive = data['life'];
  //     });
  //     this.number++;
  //     this.testData = this.number;
  //
  //   }, 25000 * 2)
  // }

  preLoader() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="loading-custom-spinner-container">
          <div class="box">
            <img src="assets/imgs/logo-round.png" alt="spinner">
          </div>
        </div>`,
      duration: 1500
    });

    loading.present();
  }

  setData(data) {
    this.GameLive = data.life;
    this.leftRoom = data.lable_1.map( index => ({
      num: index,
      state: 'inactive'
    }));
    this.reghtRoom = data.lable_2.map( index => ({
      num: index,
      state: 'inactive'
    }));
    this.GameBal = data.points;
    this.GameLevel = data.level_id;
    this.leftRoomCount = data.lable_1_step;
    this.reghtRoomCount = data.lable_2_step;
    this.timeLives = data.timer;
    this.animTimer = data.timer
  }

  ionViewWillEnter() {
    let islogin = localStorage.getItem('userData');
    if (islogin === null) {
      this.navCtrl.push(LoginPage);
    }
  }

  ionViewWillUnload() {
    let islogin = localStorage.getItem('userData');
    if (islogin === null) {
      this.navCtrl.push(LoginPage);
    }
  }

}
