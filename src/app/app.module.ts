import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';

import {AboutPage} from '../pages/about/about';
import {ContactPage} from '../pages/contact/contact';
import {HomePage} from '../pages/home/home';
import {ShopModalPage} from '../pages/shop-modal/shop-modal';
import {TimerPage} from '../pages/timer/timer';
import {LvlChangePage} from '../pages/lvl-change/lvl-change';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import { Vibration } from '@ionic-native/vibration';

import {GooglePlus} from "@ionic-native/google-plus";
import {AngularFireModule} from "angularfire2";
import firabase from 'firebase';
import {AngularFireAuth} from "angularfire2/auth";
import {HttpService} from "../provaders/http.service";
import {HttpClientModule} from "@angular/common/http";
import {GamePage} from "../pages/game/game";
import {LoginPage} from "../pages/login/login";
import {Facebook} from "@ionic-native/facebook"
import {SocialSharing} from '@ionic-native/social-sharing';
import {SoundProvider} from '../provaders/sound/sound';

export const firebaseConfig = {
  apiKey: "AIzaSyAiaYUAUOZTJzMghRyF6EpPjRIoR3YpCQw",
  authDomain: "onegameionic.firebaseapp.com",
  databaseURL: "https://onegameionic.firebaseio.com",
  projectId: "onegameionic",
  storageBucket: "onegameionic.appspot.com",
  messagingSenderId: "672138296908 "
};

firabase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    LoginPage,
    GamePage,
    ShopModalPage,
    TimerPage,
    LvlChangePage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp, {}, {
      links: [
        { component: HomePage, name: 'Home', segment: 'home' },
        { component: LoginPage, name: 'Login', segment: 'login' },
        { component: GamePage, name: 'Game', segment: 'game' },
        { component: AboutPage, name: 'About', segment: 'about' },
        { component: ShopModalPage, name: 'Shop', segment: 'shop' },
        { component: TimerPage, name: 'Time', segment: 'time' }
      ]
    }),
    AngularFireModule.initializeApp(firebaseConfig),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    GamePage,
    LoginPage,
    ShopModalPage,
    TimerPage,
    LvlChangePage
  ],
  providers: [
    StatusBar,
    AngularFireAuth,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpService,
    GooglePlus,
    Facebook,
    Vibration,
    SocialSharing,
    SoundProvider
  ]
})
export class AppModule {
}
