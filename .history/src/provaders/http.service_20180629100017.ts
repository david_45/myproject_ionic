import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';


@Injectable()

export class HttpService {
  films;


  constructor(private http: HttpClient) {
  }

  liveUpdate(social_id: any) {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.put('http://192.168.0.106/api/life/' + social_id, {headers: headers})
      .map((data) => {
        return data;
      })
  }

  getFun() {
    console.log('Hello');
    this.films = this.http.get('http://192.168.0.106/api/testing');
    this.films
      .subscribe(data => {
        console.log('my data: ', data);
      })
  }

  createUser(data) {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post('http://192.168.0.106/api/gamer', data, {headers: headers})
      .map((data) => {
        return data;
      })
  }

  Updatelevels(social_id, data) {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.put('http://192.168.0.106/api/gamer/' + social_id, data, {headers: headers})
      .map((data) => {
        return data;
      })
  }

  getTime(social_id){
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.get('http://192.168.0.106/api/timeout/' + social_id, {headers: headers})
      .map((data) => {
        return data;
      })
  }

  blockTime(social_id){
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.get('http://192.168.0.106/api/life/' + social_id, {headers: headers})
      .map((data) => {
        return data;
      })
  }

  funishBlockTime(social_id){
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.get('http://192.168.0.106/api/time/' + social_id, {headers: headers})
      .map((data) => {
        return data;
      })
  }

  getDataTest(){
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.get('http://192.168.0.106/api/gas', {headers: headers})
      .map((data) => {
        return data;
      })
  }


}
